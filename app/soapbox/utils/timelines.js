import { Map as ImmutableMap } from 'immutable';

function deep_check_reply(status) {
  //if status replies to another unreachable locked status, in_reply_to_id might be null
  //hence we have to do this deep check, which is not perfect but useful for 95% of cases
  if (status.get('mentions').isEmpty()) return false;
  const c = status.get('pleroma').get('content');
  if (!c) return false;
  const t = c.get('text/plain');
  if (!t) return false;
  return t.substring(0,1) === '@';
}

export const shouldFilter = (status, columnSettings) => {
  const shows = ImmutableMap({
    reblog: status.get('reblog') !== null,
    reply: status.get('in_reply_to_id') !== null || deep_check_reply(status),
    direct: status.get('visibility') === 'direct',
  });

  return shows.some((value, key) => {
    return columnSettings.getIn(['shows', key]) === false && value;
  });
};
